import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Side a = new Side();
        Side b = new Side();
        Side c = new Side();

        System.out.println("enter A: ");
        a.read(scanner);

        System.out.println("enter B: ");
        b.read(scanner);

        System.out.println("enter C: ");
        c.read(scanner);

        Triangle triangle = new Triangle(a, b, c);

        System.out.println("square: " + triangle.square());
        System.out.println("perimeter: " + triangle.perimeter());
    }
}