import java.util.Scanner;

class Triangle {
    private Side a, b, c;
    public Triangle(Side a_, Side b_, Side c_) {
        a = a_;
        b = b_;
        c = c_;
    }
    public double square() {
        // формула Герона
        double ab = Side.distance(a, b);
        double bc = Side.distance(b, c);
        double ac = Side.distance(a, c);
        double p = (ab + bc + ac) / 2;
        return Math.sqrt(p * (p - ab) * (p - bc) * (p - ac));
    }
    public double perimeter() {
        double ab = Side.distance(a, b);
        double bc = Side.distance(b, c);
        double ac = Side.distance(a, c);
        return ab + bc + ac;
    }
    public void print() {
        System.out.print("A: ");
        a.print();
        System.out.print("B: ");
        b.print();
        System.out.print("C: ");
        c.print();
    }
}